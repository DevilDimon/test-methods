package ru.nsu.fit.endpoint.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.UUID;

public class CustomerManagerTest {
    private DBService dbService;
    private Logger logger;
    private CustomerManager customerManager;

    private Customer customerBeforeCreateMethod;
    private Customer customerAfterCreateMethod;

    @Before
    public void before() {
        // create stubs for the test's class
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);

        customerBeforeCreateMethod = new Customer()
                .setId(null)
                .setFirstName("John")
                .setLastName("Wick")
                .setLogin("john_wick@gmail.com")
                .setPass("Baba_Jaga")
                .setBalance(0);
        customerAfterCreateMethod = customerBeforeCreateMethod.clone();
        customerAfterCreateMethod.setId(UUID.randomUUID());

        Mockito.when(dbService.createCustomer(customerBeforeCreateMethod)).thenReturn(customerAfterCreateMethod);

        // create the test's class
        customerManager = new CustomerManager(dbService, logger);
    }

    @Test
    public void testCreateNewCustomer() {
        // Вызываем метод, который хотим протестировать
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);

        // Проверяем результат выполенния метода
        Assert.assertEquals(customer.getId(), customerAfterCreateMethod.getId());

        // Проверяем, что метод мока базы данных был вызван 1 раз
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    // Как не надо писать тест...
    // Используйте expected exception аннотации...
    @Test
    public void testCreateCustomerWithNullArgument() {
        try {
            customerManager.createCustomer(null);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Argument 'customerData' is null.", ex.getMessage());
        }
    }

    // Как не надо писать тест...
    // Используйте expected exception аннотации...
    @Test
    public void testCreateCustomerWithEasyPassword() {
        try {
            customerBeforeCreateMethod.setPass("123qwe");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Password is easy.", ex.getMessage());
        }
    }

    // Lab2 tests

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithShortPassword() {
        customerBeforeCreateMethod.setPass("12345");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test
    public void testCreateCustomerWithShortestPassword() {
        customerBeforeCreateMethod.setPass("123456");
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);

        Assert.assertEquals(customer.getId(), customerAfterCreateMethod.getId());
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test
    public void testCreateCustomerWithLongestPassword() {
        customerBeforeCreateMethod.setPass("1234567890ab");
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);

        Assert.assertEquals(customer.getId(), customerAfterCreateMethod.getId());
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithLongPassword() {
        customerBeforeCreateMethod.setPass("1234567890abc");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithNullPassword() {
        customerBeforeCreateMethod.setPass(null);
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithWhitespaceInFirstName() {
        customerBeforeCreateMethod.setFirstName("J ohn");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithWhitespaceInLastName() {
        customerBeforeCreateMethod.setLastName("Wic  k");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithShortFirstName() {
        customerBeforeCreateMethod.setFirstName("J");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test
    public void testCreateCustomerWithShortestFirstName() {
        customerBeforeCreateMethod.setFirstName("Jo");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithShortLastName() {
        customerBeforeCreateMethod.setLastName("W");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test
    public void testCreateCustomerWithShortestLastName() {
        customerBeforeCreateMethod.setLastName("Wi");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithLongFirstName() {
        customerBeforeCreateMethod.setFirstName("Chesterfielde");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test
    public void testCreateCustomerWithLongestFirstName() {
        customerBeforeCreateMethod.setFirstName("Chesterfield");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithLongLastName() {
        customerBeforeCreateMethod.setLastName("Alexandrescuh");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test
    public void testCreateCustomerWithLongestLastName() {
        customerBeforeCreateMethod.setLastName("Alexandrescu");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithNotCapitalFirstLetter() {
        customerBeforeCreateMethod.setFirstName("john");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithNotCapitalLastLetter() {
        customerBeforeCreateMethod.setFirstName("wick");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithNotZeroBalance() {
        customerBeforeCreateMethod.setBalance(1);
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithFirstNameInPassword() {
        customerBeforeCreateMethod.setFirstName("Alejandro");
        customerBeforeCreateMethod.setPass("Alejandro");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithLastNameInPassword() {
        customerBeforeCreateMethod.setLastName("Kuchkovich");
        customerBeforeCreateMethod.setPass("Kuchkovich");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithNameInPassword() {
        customerBeforeCreateMethod.setLastName("kuchkovich@sobachka.gov");
        customerBeforeCreateMethod.setPass("kuchkovich@sobachka.gov");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

}
