package ru.nsu.fit.endpoint.shared;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Globals {
    public static final String ADMIN_LOGIN = "admin";
    public static final String ADMIN_PASS = "setup";

    public static final String CUSTOMER_LOGIN = "email@example.com";
    public static final String CUSTOMER_PASS = "strongpass";
}
