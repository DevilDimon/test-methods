package ru.nsu.fit.services.browser;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Please read: https://github.com/SeleniumHQ/selenium/wiki/Grid2
 *
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Browser implements Closeable {
    private WebDriver webDriver;

    public Browser() {
        // create web driver
        try {
            DesiredCapabilities dc = new DesiredCapabilities();
            dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
            System.setProperty("webdriver.chrome.driver", "./lib/chromedriver");
            webDriver = new ChromeDriver(dc);
            webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public Browser openPage(String url) {
        Reporter.log("Opening page " + url + "<br>");
        webDriver.get(url);
        makeScreenshot();
        return this;
    }

    public Browser waitForElement(By element) {
        return waitForElement(element, 10);
    }

    public Browser waitForElement(final By element, int timeoutSec) {
        WebDriverWait wait = new WebDriverWait(webDriver, timeoutSec);
        Reporter.log("Waiting for element " + element + " for " + timeoutSec + " seconds <br>");
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        makeScreenshot();
        return this;
    }

    public Browser click(By element) {
        Reporter.log("Clicking element " + element + "<br>");
        webDriver.findElement(element).click();
        makeScreenshot();
        return this;
    }

    public Browser typeText(By element, String text) {
        Reporter.log("Typing into " + element + " the text: " + text + "<br>");
        webDriver.findElement(element).sendKeys(text);
        makeScreenshot();
        return this;
    }

    public WebElement getElement(By element) {
        Reporter.log("Getting element " + element + "<br>");
        makeScreenshot();
        return webDriver.findElement(element);
    }

    public Alert getAlert() {
        return webDriver.switchTo().alert();
    }

    public String getValue(By element) {
        return getElement(element).getAttribute("value");
    }

    public List<WebElement> getElements(By element) {
        Reporter.log("Finding elements like " + element + "<br>");
        makeScreenshot();
        return webDriver.findElements(element);
    }

    public boolean isElementPresent(By element) {
        return getElements(element).size() != 0;
    }

    public String getTitle() {
        return webDriver.getTitle();
    }

    public void makeScreenshot() {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(Calendar.getInstance().getTime());
            File screenShot = new File("screenshots/"+timeStamp+".png");
            File scrFile = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, screenShot);
            String filePath = screenShot.toString();
            Reporter.log("<img src=\"../" + filePath + "\" width=1200 height=561 /><br>");
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void close() {
        webDriver.close();
    }
}
