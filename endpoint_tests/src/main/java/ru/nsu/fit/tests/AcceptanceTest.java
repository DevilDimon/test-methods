package ru.nsu.fit.tests;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.util.List;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class AcceptanceTest {
    private Browser browser = null;

    @BeforeClass
    public void beforeClass() { browser = BrowserService.openNewBrowser(); }

    @Test
    @Title("Create customer")
    @Description("Create customer via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer feature")
    public void createCustomer() {
        // login to admin cp
        browser.openPage("http://localhost:8080/endpoint");
        browser.waitForElement(By.id("email"));

        browser.typeText(By.id("email"), "admin");
        browser.typeText(By.id("password"), "setup");

        browser.click(By.id("login"));

        // create customer
        browser.click(By.id("add_new_customer"));

        browser.typeText(By.id("first_name_id"), "John");
        browser.typeText(By.id("last_name_id"), "Wick");
        browser.typeText(By.id("email_id"), "email@example.com");
        browser.typeText(By.id("password_id"), "strongpass");

        browser.click(By.id("create_customer_id"));
    }

    @Test(dependsOnMethods = "createCustomer")
    @Title("Check login")
    @Description("Get customer id by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void checkCustomer() {
        // login to admin cp
        browser.openPage("http://localhost:8080/endpoint");
        browser.waitForElement(By.id("email"));

        browser.typeText(By.id("email"), "admin");
        browser.typeText(By.id("password"), "setup");

        browser.click(By.id("login"));

        browser.waitForElement(By.tagName("tr"));

        List<WebElement> cols = browser.getElements(By.tagName("td"));
        Assert.assertEquals(cols.get(0).getText(), "John");
        Assert.assertEquals(cols.get(1).getText(), "Wick");
        Assert.assertEquals(cols.get(2).getText(), "email@example.com");
        Assert.assertEquals(cols.get(3).getText(), "strongpass");
        Assert.assertEquals(cols.get(4).getText(), "0");
    }

    @Test
    @Title("Login with invalid credentials")
    @Description("Try to login as a nonexistent customer")
    @Features("Customer feature")
    public void loginWithInvalidCredentials() {
        browser.openPage("http://localhost:8080/endpoint");
        browser.waitForElement(By.id("email"));

        browser.typeText(By.id("email"), "customer");
        browser.typeText(By.id("password"), "12345");

        try {
            browser.click(By.id("login"));
        } catch (UnhandledAlertException e) {
            Alert alert = browser.getAlert();
            Assert.assertEquals(alert.getText(), "Email or password is incorrect");
            alert.accept();
        }
    }

    @Test(dependsOnMethods = "createCustomer")
    @Title("Sort by first name")
    @Description("Try to sort by first name")
    @Features("Customer feature")
    public void checkCustomerSortingByFirstName() {
        // login to admin cp
        browser.openPage("http://localhost:8080/endpoint");
        browser.waitForElement(By.id("email"));

        browser.typeText(By.id("email"), "admin");
        browser.typeText(By.id("password"), "setup");

        browser.click(By.id("login"));

        // create another customer
        browser.click(By.id("add_new_customer"));

        browser.typeText(By.id("first_name_id"), "Dmitry");
        browser.typeText(By.id("last_name_id"), "Serov");
        browser.typeText(By.id("email_id"), "email2@example.com");
        browser.typeText(By.id("password_id"), "astrongpass");

        browser.click(By.id("create_customer_id"));

        browser.waitForElement(By.tagName("tr"));

        List<WebElement> cols = browser.getElements(By.className("sorting_1"));
        Assert.assertEquals(cols.get(0).getText(), "Dmitry");
        Assert.assertEquals(cols.get(1).getText(), "John");

        browser.click(By.className("sorting_asc"));
        cols = browser.getElements(By.className("sorting_1"));
        Assert.assertEquals(cols.get(0).getText(), "John");
        Assert.assertEquals(cols.get(1).getText(), "Dmitry");
    }

    @Test(dependsOnMethods = "checkCustomerSortingByFirstName")
    @Title("Check entry count")
    @Description("Check that there is enough entries")
    @Features("Customer feature")
    public void checkEntryCount() {
        // login to admin cp
        browser.openPage("http://localhost:8080/endpoint");
        browser.waitForElement(By.id("email"));

        browser.typeText(By.id("email"), "admin");
        browser.typeText(By.id("password"), "setup");

        browser.click(By.id("login"));

        browser.waitForElement(By.className("dataTables_info"));

        Assert.assertEquals(browser.getElement(By.className("dataTables_info")).getText(),
                "Showing 1 to 2 of 2 entries");
    }

    @Test
    @Title("Try to create customer without email")
    @Features("Admin feature")
    public void checkInvalidInputViews() {
        loginAsAdmin();

        browser.click(By.id("add_new_customer"));

        browser.typeText(By.id("first_name_id"), "Jane");
        browser.typeText(By.id("last_name_id"), "Doe");
        browser.typeText(By.id("email_id"), "");
        browser.typeText(By.id("password_id"), "strongpass");

        try {
            browser.click(By.id("create_customer_id"));
        } catch (UnhandledAlertException e) {
            Alert alert = browser.getAlert();
            alert.accept();
            WebElement emailTextField = browser.getElement(By.id("email_id"));
            Assert.assertEquals(emailTextField.getCssValue("color"), "red");
        }
    }

    private void loginAsAdmin() {
        browser.openPage("http://localhost:8080/endpoint");
        browser.waitForElement(By.id("email"));

        browser.typeText(By.id("email"), "admin");
        browser.typeText(By.id("password"), "setup");

        browser.click(By.id("login"));
    }

    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
}
