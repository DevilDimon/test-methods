package ru.nsu.fit.tests.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.services.ApiTestService;

import javax.ws.rs.core.Response;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class BuildVerificationTest {
    private ApiTestService apiService;
    private DBService dbService;

    @BeforeClass
    private void beforeClass() {
        apiService = new ApiTestService();
        Logger logger = LoggerFactory.getLogger("TEST_DB_LOG");
        dbService = new DBService(logger);
    }

    @AfterClass
    private void afterClass() {
        dbService.removeAllCustomers();
    }

    @Test(description = "Create customer via API.")
    public void createCustomer() {
        Response response = apiService.post("create_customer", "{\n" +
                "\t\"firstName\":\"Johnds\",\n" +
                "    \"lastName\":\"Weak\",\n" +
                "    \"login\":\"helloworld123@login.com\",\n" +
                "    \"pass\":\"password123\",\n" +
                "    \"balance\":\"0\"\n" +
                "}");
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
    }

    @Test(description = "Try to get the customer via API.", dependsOnMethods = "createCustomer")
    public void getCustomer() {
        Response response = apiService.get("get_customer_id/helloworld123@login.com");
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
        Customer customer = response.readEntity(Customer.class);
        Customer expectedCustomer = new Customer()
                                    .setFirstName("Johnds")
                                    .setLastName("Weak")
                                    .setLogin("helloworld123@login.com")
                                    .setPass("password123")
                                    .setBalance(0);
        Assert.assertEquals(customer.getFirstName(), expectedCustomer.getFirstName());
        Assert.assertEquals(customer.getLastName(), expectedCustomer.getLastName());
        Assert.assertEquals(customer.getLogin(), expectedCustomer.getLogin());
        Assert.assertEquals(customer.getPass(), expectedCustomer.getPass());
        Assert.assertEquals(customer.getBalance(), expectedCustomer.getBalance());
    }

    @Test(description = "Update the customer via API.", dependsOnMethods = "getCustomer")
    public void updateCustomer() {
        Response response = apiService.get("get_customer_id/helloworld123@login.com");
        Customer customer = response.readEntity(Customer.class);
        customer.setFirstName("Dmitry");
        customer.setLastName("Serov");
        response = apiService.post("update_customer", customer);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
        customer = response.readEntity(Customer.class);
        Customer expectedCustomer = new Customer()
                .setFirstName("Dmitry")
                .setLastName("Serov")
                .setLogin("helloworld123@login.com")
                .setPass("password123")
                .setBalance(0);
        Assert.assertEquals(customer.getFirstName(), expectedCustomer.getFirstName());
        Assert.assertEquals(customer.getLastName(), expectedCustomer.getLastName());
        Assert.assertEquals(customer.getLogin(), expectedCustomer.getLogin());
        Assert.assertEquals(customer.getPass(), expectedCustomer.getPass());
        Assert.assertEquals(customer.getBalance(), expectedCustomer.getBalance());
    }

    @Test(description = "Remove the customer via API.", dependsOnMethods = "createCustomer")
    public void removeCustomer() {
        Response response = apiService.post("/remove_customer/helloworld123@login.com", null);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
    }

    @Test(description = "Create plan via API")
    public void createPlan() {
        Response response = apiService.post("create_plan", "{\n" +
                "\t\"name\":\"Gigi za shagi\",\n" +
                "\t\"details\":\"Some weird Russian plan\",\n" +
                "\t\"fee\":\"52\"\n" +
                "}");
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
    }

}
